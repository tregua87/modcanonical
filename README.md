#MOD CANONICAL

This is an Apache Module that implements a defences against URL Based Dorks.

The source is **mod_canonical_set.c**
 

# INSTALLATION AND CONFIGURATION

In this section we show the main steps to install the module and how to configure it.

## Installation

To compile and install:

```
sudo make
```

To remove log files in Apache.
Every time you compile, the Makefile removes all log files.

```
sudo make clean
```

## Dependences

This moduel to work file, need of modules "setenvif" and "headers" installaed on Apache.

To activate them:
```
a2enmod setenvif
a2enmod headers
```

## Configuration
Append in the general apache configuration file ``apache2.conf``:

```
 LoadModule canonical_set_module /usr/lib/apache2/modules/mod_canonical_set.so
```

This is only e prototype!

## Virtual host configuration fils

Insert these lines in the virtual-host configuration, that is in ``site-aviable/``.

```
 <Location "/">
    # this module needs to filter header user-agent
    <IfModule mod_deflate.c>
        # redirect only for bot
        # google section
        BrowserMatchNoCase  ^.*googlebot.*$ is_bot no-gzip
        BrowserMatchNoCase  ^.*google.*$ is_bot no-gzip
        # bing section
        BrowserMatchNoCase  ^.*bingbot.*$ is_bot no-gzip
        BrowserMatchNoCase  ^.*msnbot.*$ is_bot no-gzip
        BrowserMatchNoCase  ^.*msnbot-media.*$ is_bot no-gzip
        BrowserMatchNoCase  ^.*adidxbot.*$ is_bot no-gzip
        BrowserMatchNoCase  ^.*bingpreview.*$ is_bot no-gzip
        # yahoo section
        BrowserMatchNoCase  ^.*slurp.*$ is_bot no-gzip
        # yandex section
        BrowserMatchNoCase  ^.*yandex.*$ is_bot no-gzip
        # baidu section
        BrowserMatchNoCase  ^.*baiduspider.*$ is_bot no-gzip

        # to avoid loop of redirect
        RequestHeader set NotObfuscate "1" env=!is_bot

    </IfModule>
        # these lines able own mod on target mime type
        AddOutputFilterByType CANONICAL_SET text/html
        AddOutputFilterByType CANONICAL_SET application/pdf
        AddOutputFilterByType CANONICAL_SET application/xml

        # module parameters
        SeedForTranslation 1234567890
        ExceptedUri wp-content
        #TypeObfuscation inclusion
        TypeObfuscation exclusion
        TrailerEncryption TRLR
        HeaderEncryption HEAD
        SitemapObfuscate 1
        # module on / off
        CanActivate 1
 </Location>
```


## AUTHOR

* Flavio Toffalini
* Maurizio Abba'

## LICENSE

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
