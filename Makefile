#simple make file to compile and deploy
#you need be root

all: clear
	apxs2 -i -c mod_canonical_set.c && service apache2 restart

# clear log files, easier debug
clear:
	rm -rf /var/log/apache2/*
