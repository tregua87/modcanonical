/*
=encoding utf8

=head1 NAME
 
mod_canonical_set.c -- Apache mod_canonical_set module

=head1 SYNOPSIS

 /installation:
 sudo make

 /remove log files
 sudo make clean

 /dependences
 Apache needs of modules "setenvif" and "headers".
 To activate them::
 - a2enmod setenvif
 - a2enmod headers

 /apache configuration file
 append at apache2.conf

 LoadModule canonical_set_module /usr/lib/apache2/modules/mod_canonical_set.so

 /virtual host configuration file
 into to virtual-host in site-aviable/

 <Location "/">
    # this module needs to filter header user-agent
    <IfModule mod_deflate.c>
        # redirect only for bot
        # google section
        BrowserMatchNoCase  ^.*googlebot.*$ is_bot no-gzip
        BrowserMatchNoCase  ^.*google.*$ is_bot no-gzip
        # bing section
        BrowserMatchNoCase  ^.*bingbot.*$ is_bot no-gzip
        BrowserMatchNoCase  ^.*msnbot.*$ is_bot no-gzip
        BrowserMatchNoCase  ^.*msnbot-media.*$ is_bot no-gzip
        BrowserMatchNoCase  ^.*adidxbot.*$ is_bot no-gzip
        BrowserMatchNoCase  ^.*bingpreview.*$ is_bot no-gzip
        # yahoo section
        BrowserMatchNoCase  ^.*slurp.*$ is_bot no-gzip
        # yandex section
        BrowserMatchNoCase  ^.*yandex.*$ is_bot no-gzip
        # baidu section
        BrowserMatchNoCase  ^.*baiduspider.*$ is_bot no-gzip

        # to avoid loop of redirect
        RequestHeader set NotObfuscate "1" env=!is_bot

    </IfModule>
        # these lines able own mod on target mime type
        AddOutputFilterByType CANONICAL_SET text/html
        AddOutputFilterByType CANONICAL_SET application/pdf
        AddOutputFilterByType CANONICAL_SET application/xml

        # module parameters
        SeedForTranslation 1234567890
        ExceptedUri wp-content
        #TypeObfuscation inclusion
        TypeObfuscation exclusion
        TrailerEncryption TRLR
        HeaderEncryption HEAD
        SitemapObfuscate 1
        # module on / off
        CanActivate 1
 </Location>


=head1 AUTHOR

this configuration retrive a correct TagCanonical at GoogleBot

Maurizio Abba'
Toffalini Flavio

=head1 LICENSE

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

=cut
*/

#include "httpd.h"
#include "http_config.h"
#include "util_filter.h"
#include "ap_config.h"
#include "ap_regex.h"
#include "http_log.h"
#include "apr_strmatch.h"
#include "apr_strings.h"
#include "apr_hash.h"
#include "ap_provider.h"
#include "http_core.h"
#include "http_protocol.h"
#include "http_request.h"

#define VERSION "0.2"

#define ARRAY_INIT_SZ 10

/*$1
 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    Configuration structure
 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */

typedef struct {
    char *seed;
    char * dir;
    const char * header_encryption;
    unsigned int dir_len;
    unsigned int seed_len;
    unsigned int header_encryption_len;
    const apr_strmatch_pattern *pattern_encryption_start_tag;
    const apr_strmatch_pattern *pattern_encryption_end_tag;
    const apr_strmatch_pattern *dir_pattern;
    const apr_array_header_t *excepted_uri;
    const apr_array_header_t *pattern_excepted_uri;
    const char * type_obfuscation;
    const char * trailer_encryption;
    unsigned int trailer_encryption_len;
    const char * sitemap_obfuscate;
    unsigned int activate;
} canonical_set_filter_config;

typedef struct {
    apr_bucket_brigade *bbsave;
} canonical_set_filter_ctx;

/*$1
 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    Prototypes
 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */

static const char *canonical_set_filter_name = "CANONICAL_SET";
static const char *head_start_tag = "<head>";
static const char *head_end_tag = "</head>";
static const unsigned int head_start_tag_length = 6;
static const unsigned int head_end_tag_length = 7;
static const char *link_can_start_tag = "<link";
static const char *link_can_end_tag = "/>";
static const char *link_can_middle_tag = "rel=\"canonical\"";
static const char *link_can_middle1_tag = "rel='canonical'";
static const char * link_end_tag = ">";
static const char* urlset_tag = "<urlset";
static const char* loc_start_tag = "<loc";
static const char* generic_end_tag = ">";
static const char* loc_end_tag = "</loc>";
static const char* http = "https://";
static const char* https= "http://";
static const unsigned int link_end_tag_len=1;
static const unsigned int loc_end_tag_len=6;
static const char * replace_base  = "<link rel=\"canonical\" href=\"%s://%s:%d%s%s\" /></head>";
static const char * replace_base1 = "<link rel=\"canonical\" href=\"%s://%s:%d%s%s\" />";
static const char * replace_base2 = "<%s://%s:%d%s%s>; rel=\"canonical\"";
static const char * replace_base_loc = "<loc>%s://%s:%d%s%s</loc>";
static const char * replace_base_test = "%s://%s:%d%s%s";

static const char * HEADER_NOT_OBFUSCATE = "NotObfuscate";
static const char * MY_TRUE = "1";
static const char * TO_INCLUSION = "inclusion";
static const char * TO_EXCLUSION = "exclusion";
static const char * question_mark_tag = "?";
static const char * quote = "'";
static const char * dquote = "\"";
static const char * href_attr = "href=";
static const char * html_mime = "text/html";
static const char * text_xml = "application/xml";
static const unsigned int href_attr_len = 5;
static const unsigned int generic_end_tag_len = 1;
static const apr_strmatch_pattern *pattern_head_start_tag;
static const apr_strmatch_pattern *pattern_head_end_tag;
static const apr_strmatch_pattern *pattern_generic_end_tag;
// pattern to match tag canonical
static const apr_strmatch_pattern *pattern_link_can_start_tag;
static const apr_strmatch_pattern *pattern_link_can_end_tag;
static const apr_strmatch_pattern *pattern_link_can_middle_tag;
static const apr_strmatch_pattern *pattern_link_can_middle1_tag;
static const apr_strmatch_pattern *pattern_link_end_tag;
static const apr_strmatch_pattern *pattern_question_mark_tag;
static const apr_strmatch_pattern *pattern_href_attr;
static const apr_strmatch_pattern *pattern_quote;
static const apr_strmatch_pattern *pattern_dquote;
static const apr_strmatch_pattern *pattern_html_mime;
static const apr_strmatch_pattern *pattern_text_xml;
static const apr_strmatch_pattern *pattern_urlset_tag;
static const apr_strmatch_pattern *pattern_loc_start_tag;
static const apr_strmatch_pattern *pattern_loc_end_tag;
static char * encrypt_path(apr_pool_t *p, canonical_set_filter_config *c, const char * path); //, server_rec *s);
static char * decrypt_path(apr_pool_t *p, canonical_set_filter_config *c, const char * path); //, server_rec *s);
static char * extract_uri_from_request(apr_pool_t *p, const  char *request);
static char * extract_uri_from_canonical(apr_pool_t *p, const  char *canonical);
static char * extract_uri_from_loc(apr_pool_t *p, const  char *loc, server_rec *s);
static char * match_canonical_tag(const char *l, apr_size_t len);
static int decrypt_handler(request_rec *r);
static int canonical_set_post_config(apr_pool_t *p, apr_pool_t *plog, apr_pool_t *ptemp, server_rec *s);
static apr_status_t canonical_set_out_filter(ap_filter_t *f, apr_bucket_brigade *bb);

void * create_dir_config(apr_pool_t *p, char *dir);
void * merge_dir_config(apr_pool_t *p, void *basev, void *overridesv);
static void register_hooks(apr_pool_t *p);
const char * set_header_encryption (cmd_parms *cmd, void *mconfig, const char *arg);
const char * set_trailer_encryption (cmd_parms *cmd, void *mconfig, const char *arg);
const char * set_excepted_uri(cmd_parms *cmd, void *mconfig, const char *arg);
const char * set_type_obfuscation(cmd_parms *cmd, void *mconfig, const char *arg);
const char * set_seed(cmd_parms *cmd, void *mconfig, const char *arg);
const char * set_sitemapobfuscate(cmd_parms *cmd, void *mconfig, const char *arg);
const char * set_activate(cmd_parms *cmd, void *mconfig, const char *arg);

/*$1
 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    Configuration directives
 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */

//TODO: check meaning of OR_FILEINFO
static const command_rec cmds[] = {
    AP_INIT_TAKE1("SeedForTranslation", set_seed, NULL, ACCESS_CONF, "Set the seed for url translation"),
    AP_INIT_TAKE1("HeaderEncryption", set_header_encryption, NULL, ACCESS_CONF, "Set the header for what it will be put in the url to identify encrypted urls"),
    AP_INIT_TAKE1("TrailerEncryption", set_trailer_encryption, NULL, ACCESS_CONF, "Set the Trailer for what it will be put in the url to identify encrypted urls"),
    AP_INIT_TAKE1("TypeObfuscation", set_type_obfuscation, NULL, ACCESS_CONF, "Type of obfuscation"),
    AP_INIT_ITERATE("ExceptedUri", set_excepted_uri, NULL, ACCESS_CONF, "Set a list of excepted URI"),
    AP_INIT_TAKE1("SitemapObfuscate", set_sitemapobfuscate, NULL, ACCESS_CONF, "Flag to obfuscate sitemap or not"),
    AP_INIT_TAKE1("CanActivate", set_activate, NULL, ACCESS_CONF, "Switch on/off this module, if no present the module is off"),
    {NULL}
};

/*$1
 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    Our name tag
 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */

module AP_MODULE_DECLARE_DATA canonical_set_module = {
    STANDARD20_MODULE_STUFF,
    create_dir_config,     /* create per-dir    config structures */
    merge_dir_config,      /* merge  per-dir    config structures */
    NULL,                  /* create per-server config structures */
    NULL,                  /* merge  per-server config structures */
    cmds,                  /* table of config file commands       */
    register_hooks         /* register hooks */
};


/*
 =======================================================================================================================
    Hook registration function
 =======================================================================================================================
 */


static void register_hooks(apr_pool_t *p)
{
    ap_hook_post_config(canonical_set_post_config, NULL, NULL, APR_HOOK_MIDDLE);
    ap_register_output_filter(canonical_set_filter_name, canonical_set_out_filter, NULL, AP_FTYPE_RESOURCE);
//    ap_hook_post_read_request(decrypt_handler, NULL, NULL, APR_HOOK_FIRST);
    ap_hook_translate_name(decrypt_handler, NULL, NULL, APR_HOOK_FIRST);
//    ap_hook_handler(decrypt_handler, NULL, NULL, APR_HOOK_MIDDLE);

}

/*
 =======================================================================================================================
    Filter function
 =======================================================================================================================
 */


static apr_status_t canonical_set_out_filter(ap_filter_t *f, apr_bucket_brigade *bb)
{

    ap_log_error(APLOG_MARK, APLOG_ERR, 0,f->r->server, "ENCRYPT: start set out filter!");

    request_rec *r = f->r;
    canonical_set_filter_ctx *ctx = f->ctx;
    canonical_set_filter_config *c;

    apr_bucket *b = APR_BRIGADE_FIRST(bb);

    apr_size_t bytes, bytes2;
    apr_size_t fbytes;
    apr_size_t offs;
    const char *buf;
    const char *le = NULL;
    const char *le_n;
    const char *le_r;
	
    const char *bufp;
    const char *subs;
    unsigned int match;

    apr_bucket *b1;
    apr_bucket *b2;

    char * path_uri;
    const char * complete_uri;
    const char * enc_uri;
    const char * rep;
    char *fbuf;
    int found = 0;
    apr_status_t rv;

    const char * tfilename;
    const char * exists;
    int len_path;
    char *newuri, *existinguri;
    int find_canonical = 0;// 0 => false, 1 => true, of course!!
    int is_site_map = 0;
    int is_first_log = 1;

    const char * scheme;
    const char * hostname;
    unsigned int port;

    unsigned int scheme_len;
    unsigned int hostname_len;
    unsigned int port_len;

    apr_bucket_brigade *bbline;

    if (APR_BRIGADE_EMPTY(bb)) {
        return APR_SUCCESS;
    }
    
    if (r->main) {
        ap_remove_output_filter(f);
        return ap_pass_brigade(f->next, bb);
    }

    c = ap_get_module_config(r->per_dir_config, &canonical_set_module);
    
    if (!c->activate) {
        ap_log_error(APLOG_MARK, APLOG_ERR, 0,r->server, "ENCRYPT: mod canonical not activate, leave it!!");
        return ap_pass_brigade(f->next, bb);
    }

    ap_log_error(APLOG_MARK, APLOG_ERR, 0,r->server, "ENCRYPT: initial uri: %s", r->uri);
    ap_log_error(APLOG_MARK, APLOG_ERR, 0,r->server, "ENCRYPT: initial unparsed_uri: %s", r->unparsed_uri);
    ap_log_error(APLOG_MARK, APLOG_ERR, 0,r->server, "ENCRYPT: initial the_request: %s", r->the_request);

    newuri = extract_uri_from_request(r->pool, r->the_request);

    ap_log_error(APLOG_MARK, APLOG_ERR, 0,r->server, "ENCRYPT: initial NEW URI IS: %s", newuri);

    //tfilename=r->uri;
    tfilename=newuri;
    len_path = strlen(tfilename);

    if (len_path == 0) {
        ap_log_error(APLOG_MARK, APLOG_ERR, 0,r->server, "ENCRYPT: len_path == 0, leaving");
        return APR_SUCCESS;
    }

    if (c->pattern_encryption_start_tag == NULL) {
        ap_log_error(APLOG_MARK, APLOG_ERR, 0,r->server, "ENCRYPT: c->pattern_encryption_start_tag is null ,leaving");
        return APR_SUCCESS;
    }

    ap_log_error(APLOG_MARK, APLOG_ERR, 0,r->server, "ENCRYPT: tfilename set to %s", tfilename);
    exists = apr_strmatch(c->pattern_encryption_start_tag, tfilename, len_path);
    // if (exists || apr_strnatcmp(tfilename,"/")==0 ) {
    if (exists) {
        ap_log_error(APLOG_MARK, APLOG_ERR, 0,r->server, "ENCRYPT: requested / or uri already ENC_URI, leaving");

        /*bbline = apr_brigade_create(r->pool, f->c->bucket_alloc); 
        rv = ap_pass_brigade(f->next, bbline);

        for ( b = APR_BRIGADE_FIRST(ctx->bbsave);
              b != APR_BRIGADE_SENTINEL(ctx->bbsave);
              b = APR_BUCKET_NEXT(b)) {
            apr_bucket_setaside(b, r->pool);
        }

        return rv;*/
        return APR_SUCCESS;
    }


    if (ctx == NULL) {
        ctx = f->ctx = apr_pcalloc(r->pool, sizeof(canonical_set_filter_ctx));
        ctx->bbsave = apr_brigade_create(r->pool, f->c->bucket_alloc);
    }

    scheme = ap_get_server_protocol(r->server);
    hostname = ap_get_server_name(r);
    port = ap_get_server_port(r);

    scheme_len = strlen(scheme);
    hostname_len = strlen(hostname);
    port_len = strlen(apr_psprintf(r->pool, "%d", port));

    // REDIRECT! - START
    // if i come from obfuscated URL => do nothing
    char *notObf = apr_table_get(r->headers_in, HEADER_NOT_OBFUSCATE);
    ap_log_error(APLOG_MARK, APLOG_ERR, 0,r->server, "ENCRYPT: HEADER_NOT_OBFUSCATE is %s", notObf);
    if (notObf == NULL) {

        // X-Robots-Tag: noindex
        //apr_table_set(r->headers_out, "X-Robots-Tag", "none, noarchive, noimageindex");
    	
        path_uri = apr_strmatch(c->dir_pattern, newuri, strlen(newuri));
        path_uri += c->dir_len;

        // test, forse da togliere
        /*ap_log_error(APLOG_MARK, APLOG_ERR, 0,r->server, "ENCRYPT: dirlen %s", c->dir);
        if (apr_strnatcmp(c->dir, "/") == 0) {
            existinguri += (3 + scheme_len + hostname_len) * sizeof(char);
        }*/

        ap_log_error(APLOG_MARK, APLOG_ERR, 0,r->server, "ENCRYPT: path_uri %s", path_uri);
        ap_log_error(APLOG_MARK, APLOG_ERR, 0,r->server, "ENCRYPT: args %s", r->args);

        //build the uri as <path>?<args> if there's args, otherwise just <path> 
        //complete_uri = (r->args!=NULL) ?  apr_psprintf(r->pool, "%s?%s", path_uri, r->args) :  apr_psprintf(r->pool, "%s", path_uri);
        complete_uri = apr_pstrdup(r->pool, path_uri);

        ap_log_error(APLOG_MARK, APLOG_ERR, 0,r->server, "ENCRYPT: scheme %s",  scheme);
        ap_log_error(APLOG_MARK, APLOG_ERR, 0,r->server, "ENCRYPT: hostname %s", hostname);
        ap_log_error(APLOG_MARK, APLOG_ERR, 0,r->server, "ENCRYPT: port %d", port);

        ap_log_error(APLOG_MARK, APLOG_ERR, 0,r->server, "ENCRYPT: before encrypt_path: %s", complete_uri);
        enc_uri = encrypt_path(r->pool, c, complete_uri); //, r->server);
        ap_log_error(APLOG_MARK, APLOG_ERR, 0,r->server, "ENCRYPT: after encrypt: %s", enc_uri);
        rep = apr_psprintf(r->pool, replace_base_test, scheme, hostname, port, c->dir, enc_uri);

        ap_log_error(APLOG_MARK, APLOG_ERR, 0,r->server, "ENCRYPT: new encrypt: %s", rep);

        apr_table_set(r->headers_out, "Location", rep);
        // header("HTTP/1.1 301 Moved Permanently");
        r->status = 301;
        r->status_line = apr_pstrdup(r->pool, "301 Moved Permanently");
        ap_log_error(APLOG_MARK, APLOG_ERR, 0,r->server, "ENCRYPT: before return");
    
        bbline = apr_brigade_create(r->pool, f->c->bucket_alloc); 
        rv = ap_pass_brigade(f->next, bbline);

        for ( b = APR_BRIGADE_FIRST(ctx->bbsave);
              b != APR_BRIGADE_SENTINEL(ctx->bbsave);
              b = APR_BUCKET_NEXT(b)) {
            apr_bucket_setaside(b, r->pool);
        }

        return rv;
    }
    // REDIRECT! - STOP

    ap_log_error(APLOG_MARK, APLOG_ERR, 0,r->server, "ENCRYPT: Accept-Encodyng: %s", apr_table_get(r->headers_out, "Accept-Encoding"));

    apr_table_unset(r->headers_out, "Content-Length");
    apr_table_unset(r->headers_out, "Content-MD5");
    apr_table_unset(r->headers_out, "Accept-Ranges");
    apr_table_unset(r->headers_out, "ETag");

    /*STEP 1: according to https://httpd.apache.org/docs/trunk/developer/output-filters.html
              we create a temporary brigade in order to consume a fixed amount of memory instead of consume menory proportionally to content size
              in the mean time we split the buckets at each new line*/
    bbline = apr_brigade_create(r->pool, f->c->bucket_alloc); 
    while ( b != APR_BRIGADE_SENTINEL(bb) ) {
        if ( !APR_BUCKET_IS_METADATA(b) ) {
            if ( apr_bucket_read(b, &buf, &bytes, APR_BLOCK_READ) == APR_SUCCESS ) {
                if ( bytes == 0 ) {
                    APR_BUCKET_REMOVE(b);
                }
                else {
		            while ( bytes > 0 ) {
                        le_n = memchr(buf, '\n', bytes);
                        le_r = memchr(buf, '\r', bytes);
                        if ( le_n != NULL ) {
                            if ( le_n == le_r + sizeof(char)) {le = le_n;}
                            else if ( (le_r < le_n) && (le_r != NULL) ) {le = le_r;}
                            else {le = le_n;}
                        }
                        else {le = le_r;}

                        if ( le ) {
                            offs = 1 + ((unsigned int)le-(unsigned int)buf) / sizeof(char);
                            apr_bucket_split(b, offs);
                            bytes -= offs;
                            buf += offs;
                            b1 = APR_BUCKET_NEXT(b);
                            APR_BUCKET_REMOVE(b);
                            if ( !APR_BRIGADE_EMPTY(ctx->bbsave) ) {
                                APR_BRIGADE_INSERT_TAIL(ctx->bbsave, b);
                                rv = apr_brigade_pflatten(ctx->bbsave, &fbuf, &fbytes, r->pool);
                                b = apr_bucket_pool_create(fbuf, fbytes, r->pool,r->connection->bucket_alloc);
                                apr_brigade_cleanup(ctx->bbsave);
                            }
                            APR_BRIGADE_INSERT_TAIL(bbline, b);
                                b = b1;
                        } else {
                            APR_BUCKET_REMOVE(b);
                            APR_BRIGADE_INSERT_TAIL(ctx->bbsave, b);
                            bytes = 0;
                        }
                    } // while bytes > 0 
                }
            }
            else {
    	        APR_BUCKET_REMOVE(b);
            }
        }
        else if ( APR_BUCKET_IS_EOS(b) ) {
            if ( !APR_BRIGADE_EMPTY(ctx->bbsave) ) {
                rv = apr_brigade_pflatten(ctx->bbsave, &fbuf, &fbytes, r->pool);
                b1 = apr_bucket_pool_create(fbuf, fbytes, r->pool, r->connection->bucket_alloc);
                APR_BRIGADE_INSERT_TAIL(bbline, b1);
            }
            apr_brigade_cleanup(ctx->bbsave);
            f->ctx = NULL;
            APR_BUCKET_REMOVE(b);
            APR_BRIGADE_INSERT_TAIL(bbline, b);
        } 
        else {
            apr_bucket_delete(b);
        }
        b = APR_BRIGADE_FIRST(bb);
    }

    // this need if we don't want edit the original resource
    /*rv = ap_pass_brigade(f->next, bbline);

    for ( b = APR_BRIGADE_FIRST(ctx->bbsave);
          b != APR_BRIGADE_SENTINEL(ctx->bbsave);
          b = APR_BUCKET_NEXT(b)) {
       apr_bucket_setaside(b, r->pool);
    }

    return rv;*/

    // if the request is not for HTML, append only an header
    ap_log_error(APLOG_MARK, APLOG_ERR, 0,r->server, "ENCRYPT: MIME type: %s", r->content_type);
    ap_log_error(APLOG_MARK, APLOG_ERR, 0,r->server, "ENCRYPT: SitemapOb: %s", c->sitemap_obfuscate);
    if (c->sitemap_obfuscate && apr_strnatcmp(c->sitemap_obfuscate, MY_TRUE) == 0 && apr_strmatch(pattern_text_xml, r->content_type, strlen(r->content_type))) {
        ap_log_error(APLOG_MARK, APLOG_ERR, 0,r->server, "ENCRYPT: I'm XML resource");

        /*STEP 2.1: here we apply our filter on the obtained temp brigade, the aim is obfuscate all URL*/
        ap_log_error(APLOG_MARK, APLOG_ERR, 0,r->server,"ENCRYPT: begin step 2.1");
        for ( b = APR_BRIGADE_FIRST(bbline);
            b != APR_BRIGADE_SENTINEL(bbline);
            b = APR_BUCKET_NEXT(b) ) {
            if ( !APR_BUCKET_IS_METADATA(b)
                && (apr_bucket_read(b, &buf, &bytes, APR_BLOCK_READ) == APR_SUCCESS)) {
                bufp=buf;

                if (!is_site_map) {
                    subs = apr_strmatch(pattern_urlset_tag, bufp, bytes);
                    if (subs != NULL)
                        is_site_map = 1;
                }
                if (is_site_map) {
                    if (is_first_log) {
                        ap_log_error(APLOG_MARK, APLOG_ERR, 0,r->server, "ENCRYPT: found a sitemap!");
                        is_first_log = 0;
                    }

                    subs = apr_strmatch(pattern_loc_start_tag, bufp, bytes);
                    if (subs!= NULL) {

                        ap_log_error(APLOG_MARK, APLOG_ERR, 0,r->server, "ENCRYPT: found loc tag, replace it");
                        match = ((unsigned int)subs - (unsigned int)bufp) / sizeof(char);
                        apr_bucket_split(b, match); //split at the beginning of <link
                        b1=APR_BUCKET_NEXT(b);

                        if (apr_bucket_read(b1, &bufp, &bytes2, APR_BLOCK_READ)== APR_SUCCESS) { //read next bucket in order to remove the whole tag
     	                    subs = apr_strmatch(pattern_loc_end_tag, bufp, bytes2);
			                if (subs!=NULL) {
                                match = ((unsigned int)subs - (unsigned int)bufp) / sizeof(char)+loc_end_tag_len; //include in the thing removal of >
                                apr_bucket_split(b1, match); //split at the end </loc>
	    		                b2=APR_BUCKET_NEXT(b1);
    
                			    apr_bucket_delete(b1); //delete the entire bucket <loc .... >

		    	                char* uritemp = apr_pstrmemdup(r->pool, bufp, match);

		    	                ap_log_error(APLOG_MARK, APLOG_ERR, 0,r->server, "ENCRYPT: uritemp is %s", uritemp);

                			    existinguri = extract_uri_from_loc(r->pool, uritemp, r->server);

                                    
			    	            ap_log_error(APLOG_MARK, APLOG_ERR, 0,r->server, "ENCRYPT: dirlen %s", c->dir);
                                if (apr_strnatcmp(c->dir, "/") == 0) {
                                    existinguri += (3 + scheme_len + hostname_len) * sizeof(char);
                                }

		    	                ap_log_error(APLOG_MARK, APLOG_ERR, 0,r->server, "ENCRYPT: extract url: %s", existinguri);

			                    if (existinguri != NULL) {
    
	    			                path_uri = apr_strmatch(c->dir_pattern, existinguri, strlen(existinguri));

		    		                path_uri += c->dir_len;
			    	                ap_log_error(APLOG_MARK, APLOG_ERR, 0,r->server, "ENCRYPT: path_uri %s", path_uri);
				                    ap_log_error(APLOG_MARK, APLOG_ERR, 0,r->server, "ENCRYPT: args %s", r->args);

    				                //build the uri as <path>?<args> if there's args, otherwise just <path> 
	    			                //complete_uri = (r->args!=NULL) ?  apr_psprintf(r->pool, "%s?%s", path_uri, r->args) :  apr_psprintf(r->pool, "%s", path_uri);
                                    complete_uri = apr_pstrdup(r->pool, path_uri);

                                    ap_log_error(APLOG_MARK, APLOG_ERR, 0,r->server, "ENCRYPT: scheme %s",  scheme);
                                    ap_log_error(APLOG_MARK, APLOG_ERR, 0,r->server, "ENCRYPT: hostname %s", hostname);
                                    ap_log_error(APLOG_MARK, APLOG_ERR, 0,r->server, "ENCRYPT: port %d", port);

                                    ap_log_error(APLOG_MARK, APLOG_ERR, 0,r->server, "ENCRYPT: before encrypt_path: %s", complete_uri);
                                    enc_uri = encrypt_path(r->pool, c, complete_uri); //, r->server);
                                    rep = apr_psprintf(r->pool, replace_base_loc, scheme, hostname, port, c->dir, enc_uri);
                                    ap_log_error(APLOG_MARK, APLOG_ERR, 0,r->server, "ENCRYPT: will add on page %s", rep);
                                    ap_log_error(APLOG_MARK, APLOG_ERR, 0,r->server, "ENCRYPT: composed by c->dir %s", c->dir);
                                    ap_log_error(APLOG_MARK, APLOG_ERR, 0,r->server, "ENCRYPT: composed by header_encryption %s", c->header_encryption);
                                    ap_log_error(APLOG_MARK, APLOG_ERR, 0,r->server, "ENCRYPT: composed by enc_uri %s", enc_uri);
                                    ap_log_error(APLOG_MARK, APLOG_ERR, 0,r->server, "ENCRYPT: unparsed_uri is %s", r->unparsed_uri);
                                    b1 = apr_bucket_immortal_create(rep, strlen(rep),r->connection->bucket_alloc);
                                    APR_BUCKET_INSERT_BEFORE(b2, b1); //put b1 before b2
                                    b=b2;
		    	                }
			                    else { /*EPIC FAIL*/
				                    ap_log_error(APLOG_MARK, APLOG_ERR, 0,r->server, "ENCRYPT: not find URL in LOC tag, wft?!");
		                        }
                            }   
                            else {/*EPIC FAIL*/
                                ap_log_error(APLOG_MARK, APLOG_ERR, 0,r->server, "ENCRYPT: loc end tag not found, XML is broken?");
                            }
                        }
                        else {/*EPIC FAIL*/
                            ap_log_error(APLOG_MARK, APLOG_ERR, 0,r->server, "ENCRYPT: Failure in reading bucket");
                        }
                    }
    
                }
            }
        }
    }
    else if (!apr_strmatch(pattern_html_mime, r->content_type, strlen(r->content_type))) {
        ap_log_error(APLOG_MARK, APLOG_ERR, 0,r->server, "ENCRYPT: I'm not HTML resource");
        newuri += c->dir_len;
        ap_log_error(APLOG_MARK, APLOG_ERR, 0,r->server, "ENCRYPT: uri is %s", newuri);
        // Link: <http://www.example.com/white-paper.html>; rel="canonical"
        enc_uri = encrypt_path(r->pool, c, newuri);
        apr_table_set(r->headers_out, "Link", apr_psprintf(r->pool, replace_base2, scheme, hostname, port, c->dir, enc_uri));

        ap_log_error(APLOG_MARK, APLOG_ERR, 0,r->server, "ENCRYPT: enc_uri is %s", enc_uri);
    }
    else {
        ap_log_error(APLOG_MARK, APLOG_ERR, 0,r->server, "ENCRYPT: I'm an HTML resource");

        /*STEP 2: here we apply our filter on the obtained temp brigade.
                2 operations:
                    - remove any other <link rel canonical blabla thing (if more than one SEO will discard all of them)
                    - insert our encrypted tag */
        ap_log_error(APLOG_MARK, APLOG_ERR, 0,r->server,"ENCRYPT: begin step 2");
        for ( b = APR_BRIGADE_FIRST(bbline);
            b != APR_BRIGADE_SENTINEL(bbline);
            b = APR_BUCKET_NEXT(b) ) {
            if ( !APR_BUCKET_IS_METADATA(b)
                && (apr_bucket_read(b, &buf, &bytes, APR_BLOCK_READ) == APR_SUCCESS)) {
                //THIS PART NEED TO OBFUSCATE EXISTING TAG CANONICAL
                bufp=buf;
                subs = match_canonical_tag(bufp, bytes);

                if (subs!= NULL) {

                    ap_log_error(APLOG_MARK, APLOG_ERR, 0,r->server, "ENCRYPT: found link canonical tag, replace it");
                    match = ((unsigned int)subs - (unsigned int)bufp) / sizeof(char);
                    apr_bucket_split(b, match); //split at the beginning of <link
                    b1=APR_BUCKET_NEXT(b);
                    if (apr_bucket_read(b1, &bufp, &bytes2, APR_BLOCK_READ)== APR_SUCCESS) { //read next bucket in order to remove the whole tag
     	                subs = apr_strmatch(pattern_link_end_tag, bufp, bytes2);
			            if (subs!=NULL) {
                            match = ((unsigned int)subs - (unsigned int)bufp) / sizeof(char)+link_end_tag_len; //include in the thing removal of >
                            apr_bucket_split(b1, match); //split at the end >
			                b2=APR_BUCKET_NEXT(b1);

            			    apr_bucket_delete(b1); //delete the entire bucket <link .... >

                            if (!find_canonical) {
	    		                char* uritemp = apr_pstrmemdup(r->pool, bufp, match);
    
	    	    	            ap_log_error(APLOG_MARK, APLOG_ERR, 0,r->server, "ENCRYPT: uritemp is %s", uritemp);
    
                			    existinguri = extract_uri_from_canonical(r->pool, uritemp);
    
			    	            ap_log_error(APLOG_MARK, APLOG_ERR, 0,r->server, "ENCRYPT: dirlen %s", c->dir);
                                if (apr_strnatcmp(c->dir, "/") == 0) {
                                    existinguri += (3 + scheme_len + hostname_len) * sizeof(char);
                                }

	    		                ap_log_error(APLOG_MARK, APLOG_ERR, 0,r->server, "ENCRYPT: extract url: %s", existinguri);
    
	    		                if (existinguri != NULL) {
    
				                    path_uri = apr_strmatch(c->dir_pattern, existinguri, strlen(existinguri));
			    	                path_uri += c->dir_len;
		    		                ap_log_error(APLOG_MARK, APLOG_ERR, 0,r->server, "ENCRYPT: path_uri %s", path_uri);
	    			                ap_log_error(APLOG_MARK, APLOG_ERR, 0,r->server, "ENCRYPT: args %s", r->args);
    
			    	                //build the uri as <path>?<args> if there's args, otherwise just <path> 
		    		                //complete_uri = (r->args!=NULL) ?  apr_psprintf(r->pool, "%s?%s", path_uri, r->args) :  apr_psprintf(r->pool, "%s", path_uri);
                                    complete_uri = apr_pstrdup(r->pool, path_uri);
    
                                    ap_log_error(APLOG_MARK, APLOG_ERR, 0,r->server, "ENCRYPT: scheme %s",  scheme);
                                    ap_log_error(APLOG_MARK, APLOG_ERR, 0,r->server, "ENCRYPT: hostname %s", hostname);
                                    ap_log_error(APLOG_MARK, APLOG_ERR, 0,r->server, "ENCRYPT: port %d", port);
    
                                    ap_log_error(APLOG_MARK, APLOG_ERR, 0,r->server, "ENCRYPT: before encrypt_path: %s", complete_uri);
                                    enc_uri = encrypt_path(r->pool, c, complete_uri); //, r->server);
                                    rep = apr_psprintf(r->pool, replace_base1, scheme, hostname, port, c->dir, enc_uri);
                                    ap_log_error(APLOG_MARK, APLOG_ERR, 0,r->server, "ENCRYPT: will add on page %s", rep);
                                    ap_log_error(APLOG_MARK, APLOG_ERR, 0,r->server, "ENCRYPT: composed by c->dir %s", c->dir);
                                    ap_log_error(APLOG_MARK, APLOG_ERR, 0,r->server, "ENCRYPT: composed by header_encryption %s", c->header_encryption);
                                    ap_log_error(APLOG_MARK, APLOG_ERR, 0,r->server, "ENCRYPT: composed by enc_uri %s", enc_uri);
                                    ap_log_error(APLOG_MARK, APLOG_ERR, 0,r->server, "ENCRYPT: unparsed_uri is %s", r->unparsed_uri);
                                    b1 = apr_bucket_immortal_create(rep, strlen(rep),r->connection->bucket_alloc);
                                    APR_BUCKET_INSERT_BEFORE(b2, b1); //put b1 before b2
                                    b=b2;
                                    find_canonical=1;
		    	                }
	    		                else { /*EPIC FAIL*/
				                    ap_log_error(APLOG_MARK, APLOG_ERR, 0,r->server, "ENCRYPT: not find attribute href=\"\" in tag canonica, wft?!");
		                        }
                            }
                        }
                        else {/*EPIC FAIL*/
                            ap_log_error(APLOG_MARK, APLOG_ERR, 0,r->server, "ENCRYPT: link end tag not found, HTML is broken?");
                        }
                    }
                    else {/*EPIC FAIL*/
                        ap_log_error(APLOG_MARK, APLOG_ERR, 0,r->server, "ENCRYPT: Failure in reading bucket");
                    }
                }

                bufp = buf;
                if (!find_canonical && (subs = apr_strmatch(pattern_head_end_tag, bufp, bytes)) != NULL) {
                    ap_log_error(APLOG_MARK, APLOG_ERR, 0,r->server, "ENCRYPT: found end head tag");
                    match = ((unsigned int)subs - (unsigned int)bufp) / sizeof(char); 
                    bytes -= match;
                    bufp += match;
                    apr_bucket_split(b, match); //split bucket in first part and part at <head
                    b1 = APR_BUCKET_NEXT(b); //get from </head after
                    apr_bucket_split(b1, head_end_tag_length); //split in two buckets: "<head>" and everything after
                    b2 = APR_BUCKET_NEXT(b1); //get everything after
                    apr_bucket_delete(b1); //remove "</head>"
                    bytes -= head_end_tag_length;
                    bufp += head_end_tag_length;

                    //take the path part of the uri
                    //path_uri = apr_strmatch(c->dir_pattern, r->uri, strlen(r->uri));
                    //path_uri += c->dir_len;
                    path_uri = apr_strmatch(c->dir_pattern, newuri, strlen(newuri));
                    path_uri += c->dir_len;
                    ap_log_error(APLOG_MARK, APLOG_ERR, 0,r->server, "ENCRYPT: path_uri %s", path_uri);
                    ap_log_error(APLOG_MARK, APLOG_ERR, 0,r->server, "ENCRYPT: args %s", r->args);

                    //build the uri as <path>?<args> if there's args, otherwise just <path> 
                    //complete_uri = (r->args!=NULL) ?  apr_psprintf(r->pool, "%s?%s", path_uri, r->args) :  apr_psprintf(r->pool, "%s", path_uri);
                    complete_uri = apr_pstrdup(r->pool, path_uri);

	                ap_log_error(APLOG_MARK, APLOG_ERR, 0,r->server, "ENCRYPT: before encrypt_path 2: %s", complete_uri);
                    enc_uri = encrypt_path(r->pool, c, complete_uri); //, r->server);
                    rep = apr_psprintf(r->pool, replace_base, scheme, hostname, port, c->dir, enc_uri);
                    ap_log_error(APLOG_MARK, APLOG_ERR, 0,r->server, "ENCRYPT: will add on page %s", rep);
                    ap_log_error(APLOG_MARK, APLOG_ERR, 0,r->server, "ENCRYPT: composed by c->dir %s", c->dir);
                    ap_log_error(APLOG_MARK, APLOG_ERR, 0,r->server, "ENCRYPT: composed by header_encryption %s", c->header_encryption);
                    ap_log_error(APLOG_MARK, APLOG_ERR, 0,r->server, "ENCRYPT: composed by enc_uri %s", enc_uri);
                    ap_log_error(APLOG_MARK, APLOG_ERR, 0,r->server, "ENCRYPT: unparsed_uri is %s", r->unparsed_uri);
                    b1 = apr_bucket_immortal_create(rep, strlen(rep),r->connection->bucket_alloc);
                    APR_BUCKET_INSERT_BEFORE(b2, b1); //put b1 before b2
                    b=b2;
                }
            }
        }

        // Link: <http://www.example.com/white-paper.html>; rel="canonical"
        // rep = apr_psprintf(r->pool, replace_base2, scheme, hostname, port, c->dir, enc_uri);
        // apr_table_set(r->headers_out, "Link", rep);
    
        ap_log_error(APLOG_MARK, APLOG_ERR, 0,r->server, "ENCRYPT: filter ended");

    }

    rv = ap_pass_brigade(f->next, bbline);

    for ( b = APR_BRIGADE_FIRST(ctx->bbsave);
          b != APR_BRIGADE_SENTINEL(ctx->bbsave);
          b = APR_BUCKET_NEXT(b)) {
        apr_bucket_setaside(b, r->pool);
    }

    return rv;
}

/*

 =======================================================================================================================
    Encrypt and Decrypt string function
 =======================================================================================================================
 */

// static char * encrypt_path(apr_pool_t *p,const char *pass, unsigned int len_pass, const char * path) 
static char * encrypt_path(apr_pool_t *p,canonical_set_filter_config *c, const char * path) //, server_rec *s) 
{
    char * result;
    char * path_enc;
    char * path_enc_first;
    char * path_enc_second;
    char * tres;
    int len_path;
    int cnt;

    const char *pass = c->seed;
    unsigned int len_pass = c->seed_len;

    unsigned int result_len;
    unsigned int excepted_uri_len;
    unsigned int second_piece_path;

    char * excepted_uri;
    char * partial_uri;
    unsigned int first_piece_path;

    len_path = strlen(path);

    // path empty => not encrypt
    if (len_path == 0)
       return apr_pstrcat(p, "", NULL);

    int is_partial_uri = 0;

    /*ap_log_error(APLOG_MARK, APLOG_ERR, 0, s, "ENCRYPT_PATH: start");
    ap_log_error(APLOG_MARK, APLOG_ERR, 0, s, "ENCRYPT_PATH: try to encrypt: %s", path);
    ap_log_error(APLOG_MARK, APLOG_ERR, 0, s, "ENCRYPT_PATH: try to encrypt len: %d", len_path);
    ap_log_error(APLOG_MARK, APLOG_ERR, 0, s, "ENCRYPT_PATH: excepted_uri_tot %d", c->excepted_uri->nelts);
    ap_log_error(APLOG_MARK, APLOG_ERR, 0, s, "ENCRYPT_PATH: pattern_excepted_uri_tot %d", c->pattern_excepted_uri->nelts);*/

    // test for partial uri
    int i;
    for (i = 0; i < c->excepted_uri->nelts; i++) 
       // match must be the start of string
       if ((partial_uri = apr_strmatch(APR_ARRAY_IDX(c->pattern_excepted_uri, i, const apr_strmatch_pattern *), path, len_path)) != NULL) {
           excepted_uri = APR_ARRAY_IDX(c->excepted_uri, i, const char *);
           excepted_uri_len = strlen(excepted_uri);
           is_partial_uri = 1;
           break;
       }

    //ap_log_error(APLOG_MARK, APLOG_ERR, 0, s, "ENCRYPT_PATH: end search");

    if (apr_strnatcmp(c->type_obfuscation, TO_EXCLUSION) == 0) {

        //ap_log_error(APLOG_MARK, APLOG_ERR, 0, s, "ENCRYPT_PATH: exclusion case");
        if (is_partial_uri) {
            //ap_log_error(APLOG_MARK, APLOG_ERR, 0, s, "ENCRYPT_PATH: is partial uri");

            // lenght of first part of string
            first_piece_path = ((unsigned int)partial_uri - (unsigned int)path) / sizeof(char);
            second_piece_path = len_path - (first_piece_path + excepted_uri_len);

            //ap_log_error(APLOG_MARK, APLOG_ERR, 0, s, "ENCRYPT_PATH: first_piece_path %d", first_piece_path);
            //ap_log_error(APLOG_MARK, APLOG_ERR, 0, s, "ENCRYPT_PATH: second_piece_path %d", second_piece_path);

            // string that contain encripted sub-uri
            path_enc = (char *) apr_pcalloc(p, ((2*excepted_uri_len+1)*sizeof(char)));
            tres=path_enc;

            // partial encrypt
            for (cnt=0; cnt < 2*excepted_uri_len; cnt++) {
                sprintf(tres,"%02x", partial_uri[cnt]^pass[cnt%len_pass]);
                tres+=2;
            }
            len_path = 2*excepted_uri_len;
            path_enc[2*excepted_uri_len] = '\0';

            /*ap_log_error(APLOG_MARK, APLOG_ERR, 0, s, "ENCRYPT_PATH: excepted_uri_len %d", excepted_uri_len);

            ap_log_error(APLOG_MARK, APLOG_ERR, 0, s, "ENCRYPT_PATH: encrypt end");
            ap_log_error(APLOG_MARK, APLOG_ERR, 0, s, "ENCRYPT_PATH: trailer_encr = %s", c->trailer_encryption);
            ap_log_error(APLOG_MARK, APLOG_ERR, 0, s, "ENCRYPT_PATH: trailer_encr = %d", c->trailer_encryption_len);
            ap_log_error(APLOG_MARK, APLOG_ERR, 0, s, "ENCRYPT_PATH: header_encr = %s", c->header_encryption);
            ap_log_error(APLOG_MARK, APLOG_ERR, 0, s, "ENCRYPT_PATH: header_encr_len = %d", c->header_encryption_len);
            ap_log_error(APLOG_MARK, APLOG_ERR, 0, s, "ENCRYPT_PATH: path_encr = %s", path_enc);
            ap_log_error(APLOG_MARK, APLOG_ERR, 0, s, "ENCRYPT_PATH: path_encr_len = %d", len_path);*/

            result_len = first_piece_path + c->header_encryption_len + (2*excepted_uri_len) + c->trailer_encryption_len + second_piece_path;
            result = (char *) apr_pcalloc(p, (result_len+1)*sizeof(char*));

            //ap_log_error(APLOG_MARK, APLOG_ERR, 0, s, "ENCRYPT_PATH: final_path_len = %d", result_len);

            // make a manual concat of strings
            int i;
            for (i = 0; i < result_len; i++) {
                if (i < first_piece_path)
                result[i] = path[i];

                if (i >= first_piece_path && i < first_piece_path + c->header_encryption_len)
                    result[i] = c->header_encryption[i - first_piece_path];
                if (i >= first_piece_path + c->header_encryption_len && i < first_piece_path + c->header_encryption_len + len_path)
                    result[i] = path_enc[i - first_piece_path - c->header_encryption_len];
                if (i >= first_piece_path + c->header_encryption_len + len_path && 
                    i < first_piece_path + c->header_encryption_len + len_path + c->trailer_encryption_len)
                    result[i] = c->trailer_encryption[i - first_piece_path - c->header_encryption_len - len_path];
                if (i >= first_piece_path + c->header_encryption_len + len_path + c->trailer_encryption_len)
                    result[i] = path[i - c->header_encryption_len - c->trailer_encryption_len - excepted_uri_len];
            }
        }
        else {

            //ap_log_error(APLOG_MARK, APLOG_ERR, 0, s, "ENCRYPT_PATH: is total uri");
            result = (char *) apr_pstrdup(p, path);

        }

    }
    if (apr_strnatcmp(c->type_obfuscation, TO_INCLUSION) == 0) {

        //ap_log_error(APLOG_MARK, APLOG_ERR, 0, s, "ENCRYPT_PATH: inclusion case");
        if (is_partial_uri) {
            //ap_log_error(APLOG_MARK, APLOG_ERR, 0, s, "ENCRYPT_PATH: is partial uri");

            // lenght of first and second part of string
            first_piece_path = ((unsigned int)partial_uri - (unsigned int)path) / sizeof(char);
            second_piece_path = len_path - (first_piece_path + excepted_uri_len);

            //ap_log_error(APLOG_MARK, APLOG_ERR, 0, s, "ENCRYPT_PATH: first_piece_path %d", first_piece_path);
            //ap_log_error(APLOG_MARK, APLOG_ERR, 0, s, "ENCRYPT_PATH: second_piece_path %d", second_piece_path);

            // string that contain encripted sub-uri
            path_enc_first = (char *) apr_pcalloc(p, ((2*first_piece_path+1)*sizeof(char)));
            tres=path_enc_first;

            //ap_log_error(APLOG_MARK, APLOG_ERR, 0, s, "ENCRYPT_PATH: calc lenght");

            // partial encrypt first piece
            for (cnt=0; cnt < 2*first_piece_path; cnt++) {
                sprintf(tres,"%02x", path[cnt]^pass[cnt%len_pass]);
                tres+=2;
            }
            path_enc_first[2*first_piece_path] = '\0';

            //ap_log_error(APLOG_MARK, APLOG_ERR, 0, s, "ENCRYPT_PATH: first encrypt DONE");

            path_enc_second = (char *) apr_pcalloc(p, ((2*second_piece_path+1)*sizeof(char)));
            tres=path_enc_second;

            // partial encrypt second piece
            for (cnt=0; cnt < 2*second_piece_path; cnt++) {
                sprintf(tres,"%02x", path[cnt + first_piece_path + excepted_uri_len]^pass[cnt%len_pass]);
                tres+=2;
            }
            path_enc_second[2*second_piece_path] = '\0';

            /*ap_log_error(APLOG_MARK, APLOG_ERR, 0, s, "ENCRYPT_PATH: second encrypt DONE");

            ap_log_error(APLOG_MARK, APLOG_ERR, 0, s, "ENCRYPT_PATH: path_encr_first = %s", path_enc_first);
            ap_log_error(APLOG_MARK, APLOG_ERR, 0, s, "ENCRYPT_PATH: header_encr = %s", c->header_encryption);
            ap_log_error(APLOG_MARK, APLOG_ERR, 0, s, "ENCRYPT_PATH: excepted_uri = %s", excepted_uri);
            ap_log_error(APLOG_MARK, APLOG_ERR, 0, s, "ENCRYPT_PATH: trailer_encr = %s", c->trailer_encryption);
            ap_log_error(APLOG_MARK, APLOG_ERR, 0, s, "ENCRYPT_PATH: path_encr_second = %s", path_enc_second);*/

            result = (char *) apr_pstrcat(p, path_enc_first, c->header_encryption, excepted_uri, c->trailer_encryption, path_enc_second, NULL);

            //ap_log_error(APLOG_MARK, APLOG_ERR, 0, s, "ENCRYPT_PATH: final_path_len = %d", result_len);
            //ap_log_error(APLOG_MARK, APLOG_ERR, 0, s, "ENCRYPT_PATH: final_path = %s", result);

            // make a manual concat of strings
            /*for (int i = 0; i < result_len; i++) {
                if (i < first_piece_path)
                    result[i] = path[i];
                if (i >= first_piece_path && i < first_piece_path + c->header_encryption_len)
                    result[i] = c->header_encryption[i - first_piece_path];
                if (i >= first_piece_path + c->header_encryption_len && i < first_piece_path + c->header_encryption_len + len_path)
                    result[i] = path_enc[i - first_piece_path - c->header_encryption_len];
                if (i >= first_piece_path + c->header_encryption_len + len_path && 
                    i < first_piece_path + c->header_encryption_len + len_path + c->trailer_encryption_len)
                    result[i] = c->trailer_encryption[i - first_piece_path - c->header_encryption_len - len_path];
                if (i >= first_piece_path + c->header_encryption_len + len_path + c->trailer_encryption_len)
                    result[i] = path[i - c->header_encryption_len - c->trailer_encryption_len - excepted_uri_len];
            }*/
        }
        else {

            path_enc = (char *) apr_pcalloc(p, ((len_path*2+1)*sizeof(char)));
            tres=path_enc;
            //ap_log_error(APLOG_MARK, APLOG_ERR, 0, s, "ENCRYPT_PATH: is total uri");
            // total encrypt
            for (cnt=0; cnt < len_path; cnt++) {
                sprintf(tres,"%02x", path[cnt]^pass[cnt%len_pass]);
                tres+=2;
            }
            path_enc[(len_path*2)]='\0';
            len_path = strlen(path_enc);

            /*ap_log_error(APLOG_MARK, APLOG_ERR, 0, s, "ENCRYPT_PATH: encrypt end");
            ap_log_error(APLOG_MARK, APLOG_ERR, 0, s, "ENCRYPT_PATH: trailer_encr = %s", c->trailer_encryption);
            ap_log_error(APLOG_MARK, APLOG_ERR, 0, s, "ENCRYPT_PATH: trailer_encr = %d", c->trailer_encryption_len);
            ap_log_error(APLOG_MARK, APLOG_ERR, 0, s, "ENCRYPT_PATH: header_encr = %s", c->header_encryption);
            ap_log_error(APLOG_MARK, APLOG_ERR, 0, s, "ENCRYPT_PATH: header_encr_len = %d", c->header_encryption_len);
            ap_log_error(APLOG_MARK, APLOG_ERR, 0, s, "ENCRYPT_PATH: path_encr = %s", path_enc);
            ap_log_error(APLOG_MARK, APLOG_ERR, 0, s, "ENCRYPT_PATH: path_encr_len = %d", len_path);*/

            result_len = len_path+c->header_encryption_len+c->trailer_encryption_len;
            result = (char *) apr_pcalloc(p, (result_len+1)*sizeof(char*));

            //ap_log_error(APLOG_MARK, APLOG_ERR, 0, s, "ENCRYPT_PATH: result_len = %d", result_len);

            //ap_log_error(APLOG_MARK, APLOG_ERR, 0, s, "ENCRYPT_PATH: start copy");

            // make a manual concat of strings
            int i;
            for (i = 0; i < result_len; i++) {
                if (i < c->header_encryption_len)
                    result[i] = c->header_encryption[i];
                if (i >= c->header_encryption_len && i < c->header_encryption_len + c->trailer_encryption_len)
                    result[i] = c->trailer_encryption[i - c->header_encryption_len];
                if (i >= c->header_encryption_len + c->trailer_encryption_len && i < c->header_encryption_len + c->trailer_encryption_len + len_path)
                    result[i] = path_enc[i - c->header_encryption_len - c->trailer_encryption_len];
                
                /*if (i < c->header_encryption_len)
                    result[i] = c->header_encryption[i];
                if (i >= c->header_encryption_len && i < c->header_encryption_len + len_path)
                    result[i] = path_enc[i - c->header_encryption_len];
                if (i >= c->header_encryption_len + len_path)
                    result[i] = c->trailer_encryption[i - c->header_encryption_len - len_path];*/
                
            }
            //ap_log_error(APLOG_MARK, APLOG_ERR, 0, s, "ENCRYPT_PATH: after concat");

            result[result_len]='\0';
        }

    }

    //ap_log_error(APLOG_MARK, APLOG_ERR, 0, s, "ENCRYPT_PATH: before result");
    return result;
}

static char * decrypt_path(apr_pool_t *p, canonical_set_filter_config *c, const char * path) //, server_rec *s) 
{
    char * result;
    char * tres;
    char * tpath;
    unsigned int cnt;
    unsigned int tlett;
    unsigned int len_path;
    char * subs_header;
    char * subs_trailer;
    unsigned int first_piece_path;
    unsigned int second_piece_path;
    unsigned int trailer_path;
    unsigned int trailer_path2;
    char * uri_enc;
    unsigned int uri_enc_len;
    char * uri;
    unsigned int uri_len;

    char * first;
    char * second;
    char * first_plain;
    char * second_plain;

    unsigned int end_path;

    const char *pass = c->seed;
    unsigned int len_pass = c->seed_len;

    unsigned int first_piece_len;
    unsigned int second_piece_len;
    unsigned int first_piece_enc_len;
    unsigned int second_piece_enc_len;

    int is_known_path = 0;

    //ap_log_error(APLOG_MARK, APLOG_ERR, 0, s, "DECRYPT_PATH: i try decrypt %s", path);

    len_path = strlen(path);
    subs_header = apr_strmatch(c->pattern_encryption_start_tag, path, len_path);
    subs_trailer = apr_strmatch(c->pattern_encryption_end_tag, path, len_path);

    // check integrity on hedaer and trailer
    if (subs_header == NULL || subs_trailer == NULL || subs_header >= subs_trailer)
        return NULL;

    int inclusion_total = 0; // this flag needs when the module is configurate in inclusion mod and the url si totally obfuscated
    // test if the header is just before the trailer (there is not string between them) and the header is at start of the URL
    if (subs_header + c->header_encryption_len*sizeof(char) == subs_trailer && subs_header == path) {
        //ap_log_error(APLOG_MARK, APLOG_ERR, 0, s, "DECRYPT_PATH: inclusion_total !!");
        inclusion_total = 1; 
    }
    /*else {
        ap_log_error(APLOG_MARK, APLOG_ERR, 0, s, "DECRYPT_PATH: NOT inclusion_total!");
    }*/
    
    if (inclusion_total) {
        
        first = apr_pstrdup(p, "");
        second = apr_pstrdup(p, subs_trailer + c->trailer_encryption_len*sizeof(char));
        uri_enc = apr_pstrdup(p, "");

        first_piece_path = 0;

        is_known_path = 1;

    }
    else {
        //ap_log_error(APLOG_MARK, APLOG_ERR, 0, s, "DECRYPT_PATH: after first test");
  
        first_piece_path = ((unsigned int)subs_header - (unsigned int)path) / sizeof(char);
        trailer_path = ((unsigned int)subs_trailer - (unsigned int)subs_header) / sizeof(char);
        trailer_path2 = ((unsigned int)subs_trailer - (unsigned int)path) / sizeof(char);

        uri_enc = apr_pstrndup(p, subs_header + c->header_encryption_len*sizeof(char), trailer_path - c->trailer_encryption_len);
        uri_enc_len = strlen(uri_enc);

        first = apr_pstrndup(p, path, first_piece_path);
        second = apr_pstrdup(p, subs_trailer + c->trailer_encryption_len);

        //ap_log_error(APLOG_MARK, APLOG_ERR, 0, s, "DECRYPT_PATH: uri_enc %s", uri_enc);
        //ap_log_error(APLOG_MARK, APLOG_ERR, 0, s, "DECRYPT_PATH: uri_enc_len %d", uri_enc_len);

        // check if middle path is known
        is_known_path = 0;
        int i;
        for (i = 0; i < c->excepted_uri->nelts; i++) 
            if (apr_strnatcmp(APR_ARRAY_IDX(c->excepted_uri, i, const char* ), uri_enc) == 0) {
                is_known_path = 1;
                break;
            }
    }

    if (is_known_path) {

        /*ap_log_error(APLOG_MARK, APLOG_ERR, 0, s, "DECRYPT_PATH: is known path");
        ap_log_error(APLOG_MARK, APLOG_ERR, 0, s, "DECRYPT_PATH: first %s", first);
        ap_log_error(APLOG_MARK, APLOG_ERR, 0, s, "DECRYPT_PATH: second %s", second);
        ap_log_error(APLOG_MARK, APLOG_ERR, 0, s, "DECRYPT_PATH: uri_enc %s", uri_enc);*/

        first_piece_enc_len = first_piece_path;
        second_piece_enc_len = strlen(second);

        if (first_piece_enc_len%2 != 0 || second_piece_enc_len%2 != 0)
            return NULL;

        first_piece_len = first_piece_enc_len / 2;
        first_plain = (char *) apr_pcalloc(p,(first_piece_len+1)*sizeof(char));
        tres=first_plain;
        tpath = first;
        for (cnt=0; cnt < first_piece_len; cnt++) {
            sscanf(tpath, "%2x", &tlett);
            sprintf(tres,"%c", tlett^pass[cnt%len_pass]);
            tres+=1;
            tpath+=2;
        }
        first_plain[first_piece_len]='\0';

        //ap_log_error(APLOG_MARK, APLOG_ERR, 0, s, "DECRYPT_PATH: first_plain %s", first_plain);
        //ap_log_error(APLOG_MARK, APLOG_ERR, 0, s, "DECRYPT_PATH: second %s", second);

        second_piece_len = second_piece_enc_len / 2;
        second_plain = (char *) apr_pcalloc(p,(second_piece_len+1)*sizeof(char));
        tres=second_plain;
        tpath = second;
        for (cnt=0; cnt < second_piece_len; cnt++) {
            sscanf(tpath, "%2x", &tlett);
            sprintf(tres,"%c", tlett^pass[cnt%len_pass]);
            tres+=1;
            tpath+=2;
        }
        second_plain[second_piece_len]='\0';

        //ap_log_error(APLOG_MARK, APLOG_ERR, 0, s, "DECRYPT_PATH: second_plain %s", second_plain);

        return apr_pstrcat(p, first_plain, uri_enc, second_plain, NULL);
    }
    else {

        /*ap_log_error(APLOG_MARK, APLOG_ERR, 0, s, "DECRYPT_PATH: unknown path");
        ap_log_error(APLOG_MARK, APLOG_ERR, 0, s, "DECRYPT_PATH: uri_enc %s", uri_enc);
        ap_log_error(APLOG_MARK, APLOG_ERR, 0, s, "DECRYPT_PATH: first %s", first);
        ap_log_error(APLOG_MARK, APLOG_ERR, 0, s, "DECRYPT_PATH: second %s", second);*/

        uri_enc_len = strlen(uri_enc);
        if (uri_enc_len%2!= 0)
            return NULL;
        uri_len = uri_enc_len / 2;
        uri = (char *) apr_pcalloc(p,(uri_len+1)*sizeof(char));
        tres=uri;
        tpath = uri_enc;
        for (cnt=0; cnt < uri_enc_len/2; cnt++) {
            sscanf(tpath, "%2x", &tlett);
            sprintf(tres,"%c", tlett^pass[cnt%len_pass]);
            tres+=1;
            tpath+=2;
        }
        uri[uri_len]='\0';
        //ap_log_error(APLOG_MARK, APLOG_ERR, 0, s, "DECRYPT_PATH: uri dec is %s", uri);

        // test uri is in matched uri, i don't konw if this test is necessary!!
        /*if (apr_strnatcmp(first, "") != 0 && apr_strnatcmp(second, "") != 0) {
            is_known_path = 0;
            for (int i = 0; i < c->excepted_uri_tot; i++) 
                if (apr_strnatcmp(c->excepted_uri[i], uri) == 0) {
                    is_known_path = 1;
                    break;
                }
  
            if (!is_known_path)
                return NULL;
        }*/

        return apr_pstrcat(p, first, uri, second, NULL);
    }

    return NULL;
}

static char * match_canonical_tag(const char *l, apr_size_t len) { 

  char *start, *end, *middle;
  unsigned int match_start, match_end;

 //test tag <link
  while((start = apr_strmatch(pattern_link_can_start_tag, l, len)) != NULL) {

    match_start = ((unsigned int)start - (unsigned int)l) / sizeof(char);

    len -= match_start;
    l += match_start;

    if ((end = apr_strmatch(pattern_link_can_end_tag, l, len)) == NULL)
       return NULL;

    if ((middle = apr_strmatch(pattern_link_can_middle_tag, l, len)) == NULL)
       if ((middle = apr_strmatch(pattern_link_can_middle1_tag, l, len)) == NULL)
          return NULL;

    if (middle >= start && middle <= end)
       return start;

    match_end = ((unsigned int)end - (unsigned int)l) / sizeof(char);

    len -= match_end;
    l += match_end;

  }

  return NULL;
}

static char * extract_uri_from_loc(apr_pool_t *p, const  char *loc, server_rec *s) {
// expeted tag is: <loc>http[s]://www.somesite.com/somedir/plainuri</loc>
// return will be: http[s]://www.somesite.com/somedir/plainuri

    ap_log_error(APLOG_MARK, APLOG_ERR, 0, s, "EXTRACT: loc tag: %s", loc);

    int len = strlen(loc);
    int cnt;
    char *start, *end, *result, *tres, *tpath;
    unsigned int match_start, match_end;

    //extract >
    if ((start = apr_strmatch(pattern_generic_end_tag, loc, len)) != NULL) {

        match_start = ((unsigned int)start - (unsigned int)loc) / sizeof(char);

        len -= match_start-generic_end_tag_len;
        loc += match_start+generic_end_tag_len;

        if ((end = apr_strmatch(pattern_loc_end_tag, loc, len)) == NULL)
            return NULL;

        match_end = ((unsigned int)end - (unsigned int)loc) / sizeof(char);

        return apr_pstrmemdup(p, loc, match_end);
    }

    return NULL;
}

static char * extract_uri_from_canonical(apr_pool_t *p, const  char *canonical) {
  
  //ap_log_error(APLOG_MARK, APLOG_ERR, 0, s, "EXTRACT: canonical tag: %s", canonical);

  int len = strlen(canonical);
  int cnt;
  char *start, *end, *result, *tres, *tpath;
  unsigned int match_start, match_end;

  //extract attr href= 
  if ((start = apr_strmatch(pattern_href_attr, canonical, len)) != NULL) {

    match_start = ((unsigned int)start - (unsigned int)canonical) / sizeof(char);

    // add and sub 1 because i don't considerete character " and ' in match
    len -= match_start-1-href_attr_len;
    canonical += match_start+1+href_attr_len;

    if ((end = apr_strmatch(pattern_dquote, canonical, len)) == NULL)
       if ((end = apr_strmatch(pattern_quote, canonical, len)) == NULL)
          return NULL;

    match_end = ((unsigned int)end - (unsigned int)canonical) / sizeof(char);

    return apr_pstrmemdup(p, canonical, match_end);
  }

  return NULL;
}

static char * extract_uri_from_request(apr_pool_t *p, const  char *request) {

  unsigned int len_request;
  char *result;
  char *first_space;
  char *second_space;
  int idx_first_space;
  int idx_second_space;
  int len_result;

  //ap_log_error(APLOG_MARK, APLOG_ERR, 0, s, "ENCRYPT: extract_uri, the_request: %s", request);
  first_space = strchr(request, ' ');

  //ap_log_error(APLOG_MARK, APLOG_ERR, 0, s, "ENCRYPT: extract_uri, first_space: %c", *first_space);

  if (first_space == NULL)
    return NULL;

  idx_first_space = (int)(first_space - request);

  //ap_log_error(APLOG_MARK, APLOG_ERR, 0, s, "ENCRYPT: extract_uri, idx_first_space: %d", idx_first_space);

  second_space = strchr(first_space+1, ' ');

  //ap_log_error(APLOG_MARK, APLOG_ERR, 0, s, "ENCRYPT: extract_uri, second_space: %c", *second_space);

  if (second_space == NULL)
    return NULL;

  idx_second_space = (int)(second_space - request);

  //ap_log_error(APLOG_MARK, APLOG_ERR, 0, s, "ENCRYPT: extract_uri, idx_second_space: %d", idx_second_space);

  len_result = (idx_second_space - idx_first_space)*sizeof(char);

  //ap_log_error(APLOG_MARK, APLOG_ERR, 0, s, "ENCRYPT: extract_uri, len_result: %d", len_result);

  result = (char *) apr_pcalloc(p, len_result);

  int i;
  for(i = 0; i < len_result; i++)
     result[i] = request[i+idx_first_space+1];

  result[len_result-1]='\0';

  //ap_log_error(APLOG_MARK, APLOG_ERR, 0, s, "ENCRYPT: extract_uri, result: %s", result);

  return result;

}
/*
 =======================================================================================================================
    Handler function
 =======================================================================================================================
 */

static int decrypt_handler(request_rec *r) 
{

    const char *subs;
    char * enc_url;
    char * tfilename;
    canonical_set_filter_config * c = (canonical_set_filter_config *) ap_get_module_config(r->per_dir_config, &canonical_set_module);
    unsigned int len_path;
    char * rpath;

    ap_log_error(APLOG_MARK, APLOG_ERR, 0,r->server, "DECRYPT: mod canonical!!");

    if (!c->activate) {
        ap_log_error(APLOG_MARK, APLOG_ERR, 0,r->server, "DECRYPT: mod canonical deactive, leave it!");
        return DECLINED;
    }

    ap_log_error(APLOG_MARK, APLOG_ERR, 0,r->server, "DECRYPT: initial uri: %s", r->uri);
    ap_log_error(APLOG_MARK, APLOG_ERR, 0,r->server, "DECRYPT: initial args: %s", r->args);
    
    // do nothing with internal request
    if (r->prev != NULL) {
        ap_log_error(APLOG_MARK, APLOG_ERR, 0,r->server, "DECRYPT: internal request, do nothing");
        return DECLINED;
    }

    if (r->uri[0]=='*' && r->uri[1]=='\0') {
     	ap_log_error(APLOG_MARK, APLOG_ERR, 0,r->server, "DECRYPT: asked for *, leave it");
        apr_table_set(r->headers_in,HEADER_NOT_OBFUSCATE, MY_TRUE);
        return DECLINED;
    }

   /*if (c->pattern_encryption_start_tag == NULL) {
                ap_log_error(APLOG_MARK, APLOG_ERR, 0,r->server, "DECRYPT: c->pattern_encryption_start_tag is null, leave it");
                return DECLINED;
            }*/

    tfilename = extract_uri_from_request(r->pool, r->the_request);
    //tfilename = apr_pstrdup(r->pool, r->uri+c->dir_len);

    if (strlen(tfilename) == 0) {
        ap_log_error(APLOG_MARK, APLOG_ERR, 0,r->server, "DECRYPT: tfilename len == 0, leave it");
        apr_table_set(r->headers_in, HEADER_NOT_OBFUSCATE, MY_TRUE);
        return DECLINED;
    }

    //if the request is just the /, don't do anything 
    ap_log_error(APLOG_MARK, APLOG_ERR, 0,r->server, "DECRYPT: before branch");
    ap_log_error(APLOG_MARK, APLOG_ERR, 0,r->server, "DECRYPT: tfilename = %s", tfilename);
    if (apr_strnatcmp(tfilename, "/")!=0) {
        ap_log_error(APLOG_MARK, APLOG_ERR, 0,r->server, "branch top");
        
        len_path = strlen(tfilename);
        ap_log_error(APLOG_MARK, APLOG_ERR, 0,r->server, "DECRYPT: before test lenght");
        ap_log_error(APLOG_MARK, APLOG_ERR, 0,r->server, "DECRYPT: len_path = %d", len_path);
        ap_log_error(APLOG_MARK, APLOG_ERR, 0,r->server, "DECRYPT: tfilename = %s", tfilename);
        //if (len_path == 0) {
        //    tfilename = apr_pstrdup(r->pool, "");
        //}
        /*if (tfilename[len_path-1]=='/') {
            tfilename[len_path-1]='\0';
            len_path-=1;
        }*/
        ap_log_error(APLOG_MARK, APLOG_ERR, 0,r->server, "DECRYPT: after test lenght");
        ap_log_error(APLOG_MARK, APLOG_ERR, 0,r->server, "DECRYPT: tfilename set to %s", tfilename);
        //look for ENC_URL path
        ap_log_error(APLOG_MARK, APLOG_ERR, 0,r->server, "DECRYPT: c->header_encryption = %s", c->header_encryption);
    	subs = NULL;
	    if (c->pattern_encryption_start_tag == NULL) {
	        ap_log_error(APLOG_MARK, APLOG_ERR, 0,r->server, "DECRYPT: c->header_encryption is null n2, leav it (??)");
            apr_table_set(r->headers_in, HEADER_NOT_OBFUSCATE, MY_TRUE);
	        return DECLINED;
    	}
        subs = apr_strmatch(c->pattern_encryption_start_tag, tfilename, len_path);        
        ap_log_error(APLOG_MARK, APLOG_ERR, 0,r->server, "DECRYPT: subs value:  %s", subs);
        if (subs != NULL) {
            //we found an ENC_URL, hurray!!
            //enc_url = apr_pstrdup(r->pool,tfilename+c->header_encryption_len);
            enc_url = apr_pstrdup(r->pool, tfilename+c->dir_len);
            // its a test
            /*if (apr_strnatcmp(c->dir, "/") != 0) {
                enc_url += c->dir_len * sizeof(char);
            }*/

            rpath = decrypt_path(r->pool, c, enc_url); //, r->server);

            ap_log_error(APLOG_MARK, APLOG_ERR, 0,r->server, "DECRYPT: the request before enc:  %s", r->the_request);

            // the decryption failed
            if (rpath == NULL) {
                ap_log_error(APLOG_MARK, APLOG_ERR, 0, r->server, "DECRYPT: the decrypt failed");
                //apr_table_set(r->headers_in, HEADER_NOT_OBFUSCATE, MY_TRUE);
    	        return DECLINED;
            }

            ap_log_error(APLOG_MARK, APLOG_ERR, 0, r->server, "DECRYPT: the rpath is %s", rpath);
	        char *myuri = apr_pstrcat(r->pool, c->dir, rpath, NULL);
            ap_log_error(APLOG_MARK, APLOG_ERR, 0, r->server, "DECRYPT: myuri is %s", myuri);
            // r->uri = apr_pstrcat(r->pool, c->dir,rpath,NULL);
            r->uri = apr_pstrdup(r->pool, myuri);
	        r->unparsed_uri = apr_pstrdup(r->pool, myuri);
	        r->the_request = apr_pstrcat(r->pool, r->method, " ", myuri ," ", r->protocol, NULL);		
 
 	        ap_core_translate(r);
 	   
            subs = apr_strmatch(pattern_question_mark_tag, rpath, strlen(rpath));
	
            ap_log_error(APLOG_MARK, APLOG_ERR, 0,r->server, "DECRYPT: the request:  %s", r->the_request);
            if (subs != NULL) { //WE HAVE SOME ARGS other than the url
                 r->args = apr_pstrdup(r->pool, subs+sizeof(char)); //remove ?
                 r->uri[subs-rpath+sizeof(char)]='\0';
            } 

            ap_log_error(APLOG_MARK, APLOG_ERR, 0,r->server, "DECRYPT: new uri: %s", r->uri);
            ap_log_error(APLOG_MARK, APLOG_ERR, 0,r->server, "DECRYPT: new args: %s", r->args);
            //we changed the url, return gracefully
            apr_table_set(r->headers_in, HEADER_NOT_OBFUSCATE, MY_TRUE);
	        return DECLINED;
        }
        else {
            //if path does not have the ENC_URL tag, decline so that it can go wherever it wants
            ap_log_error(APLOG_MARK, APLOG_ERR, 0,r->server, "DECRYPT: subs is null, tfilename should be not set");

            if (tfilename != NULL) {

                // if i have other parameters i need to append them at tfilname!
                // if (r->)

                ap_log_error(APLOG_MARK, APLOG_ERR, 0,r->server, "DECRYPT: tfilename = %s", tfilename);

                // i need to understand if the original URL should be obfuscate or not
                enc_url = encrypt_path(r->pool, c, tfilename);

                ap_log_error(APLOG_MARK, APLOG_ERR, 0,r->server, "DECRYPT: enc_uri = %s", enc_url);

                if (apr_strnatcmp(enc_url, tfilename) != 0)
                    apr_table_unset(r->headers_in, HEADER_NOT_OBFUSCATE);
                else
                    apr_table_set(r->headers_in, HEADER_NOT_OBFUSCATE, MY_TRUE);
            }

            //apr_table_set(r->headers_in, HEADER_NOT_OBFUSCATE, MY_TRUE);
            return DECLINED;
        }
    }
    else {
		ap_log_error(APLOG_MARK, APLOG_ERR, 0,r->server, "branch bottom");
        //asked for /, we decline the request so that it can be supported by somebody else
        ap_log_error(APLOG_MARK, APLOG_ERR, 0,r->server, "DECRYPT: asked for /, leave it");
        apr_table_set(r->headers_in, HEADER_NOT_OBFUSCATE, MY_TRUE);
        return DECLINED;
    }
    //here should be impossible to go, anyway if we are here we for sure don't want to do anything
    apr_table_set(r->headers_in, HEADER_NOT_OBFUSCATE, MY_TRUE);
    return DECLINED;
}

void * create_dir_config(apr_pool_t *p, char *dir)
{
    canonical_set_filter_config *c = apr_pcalloc(p, sizeof(canonical_set_filter_config));
    if (dir == NULL) {
        c->dir = apr_pstrdup(p, "/");
    }
   else {
        /* make sure it has a trailing slash */
        if (dir[strlen(dir)-1] == '/') {
            c->dir = apr_pstrdup(p, dir);
        }
        else {
            c->dir = apr_pstrcat(p, dir, "/", NULL);
        }
    }
    c->dir_len = strlen(c->dir);
    c->dir_pattern = apr_strmatch_precompile(p, c->dir, 0);
    c->seed = NULL;
    c->seed_len =0;
    c->header_encryption = NULL;
    c->header_encryption_len = 0;
    c->trailer_encryption = NULL;
    c->trailer_encryption_len = 0;
    c->type_obfuscation = apr_pstrdup(p, TO_INCLUSION);

    c->excepted_uri = apr_array_make(p, ARRAY_INIT_SZ, sizeof(const char*));
    c->pattern_excepted_uri = apr_array_make(p, ARRAY_INIT_SZ, sizeof(const apr_strmatch_pattern*));

    c->activate = 0;

    return (void *) c;
}

void * merge_dir_config(apr_pool_t *p, void *basev, void *overridesv)
{
    canonical_set_filter_config *c = (canonical_set_filter_config *) apr_palloc(p, sizeof(canonical_set_filter_config));
    canonical_set_filter_config *base = (canonical_set_filter_config *)basev;
    canonical_set_filter_config *overrides = (canonical_set_filter_config *)overridesv;

    c->seed = overrides->seed ? overrides->seed : base->seed;
    c->seed_len = overrides->seed_len ? overrides->seed_len : base->seed_len;
    c->dir = overrides->dir ? overrides->dir : base->dir;
    c->dir_len = overrides->dir_len ? overrides->dir_len : base->dir_len;
    c->dir_pattern = overrides->dir_pattern ? overrides->dir_pattern : base->dir_pattern;
    c->header_encryption = overrides->header_encryption ? overrides->header_encryption : base->header_encryption;
    c->header_encryption_len = overrides->header_encryption_len ? overrides->header_encryption_len : base->header_encryption_len;
    c->pattern_encryption_start_tag = overrides->pattern_encryption_start_tag ? overrides->pattern_encryption_start_tag : base->pattern_encryption_start_tag;    

    c->excepted_uri = overrides->excepted_uri ? overrides->excepted_uri : base->excepted_uri;
    c->pattern_excepted_uri = overrides->pattern_excepted_uri ? overrides->pattern_excepted_uri : base->pattern_excepted_uri;
    //c->excepted_uri_tot = overrides->excepted_uri_tot ? overrides->excepted_uri_tot : base->excepted_uri_tot;

    //c->excepted_uri2 = overrides->excepted_uri2 ? overrides->excepted_uri2 : base->excepted_uri2;

    c->type_obfuscation = overrides->type_obfuscation ? overrides->type_obfuscation : base->type_obfuscation;

    c->trailer_encryption = overrides->trailer_encryption? overrides->trailer_encryption : base->trailer_encryption;
    c->trailer_encryption_len = overrides->trailer_encryption_len ? overrides->trailer_encryption_len : base->trailer_encryption_len;
    c->pattern_encryption_end_tag = overrides->pattern_encryption_end_tag ? overrides->pattern_encryption_end_tag : base->pattern_encryption_end_tag;    
    c->activate = overrides->activate ? overrides->activate : base->activate;

    c->sitemap_obfuscate = overrides->sitemap_obfuscate ? overrides->sitemap_obfuscate : base->sitemap_obfuscate;

    return (void *) c;
}

const char * set_seed(cmd_parms *cmd, void *mconfig, const char *arg)
{
    canonical_set_filter_config *c = (canonical_set_filter_config *)mconfig;
    // TODO check seed that is a string 
    if (arg!=NULL) {
        c->seed = apr_pstrdup(cmd->pool, arg);
    }
    else {
        c->seed = apr_pstrdup(cmd->pool, "AAAA");
    }
    c->seed_len = strlen(c->seed);
    return NULL;
}

const char * set_header_encryption (cmd_parms *cmd, void *mconfig, const char *arg)
{
    canonical_set_filter_config *c = (canonical_set_filter_config *)mconfig;
    // TODO check seed that is a string 
    if (arg!= NULL) {
        c->header_encryption = apr_pstrdup(cmd->pool, arg);
    }
    else {
        c->header_encryption = apr_pstrdup(cmd->pool, "BBBBB");
    }
    c->header_encryption_len = strlen(c->header_encryption);
    c->pattern_encryption_start_tag = apr_strmatch_precompile(cmd->pool, c->header_encryption, 0);
    return NULL;
}

const char * set_trailer_encryption (cmd_parms *cmd, void *mconfig, const char *arg)
{
    canonical_set_filter_config *c = (canonical_set_filter_config *)mconfig;
    // TODO check seed that is a string 
    if (arg!= NULL) {
        c->trailer_encryption = apr_pstrdup(cmd->pool, arg);
    }
    else {
        c->trailer_encryption = apr_pstrdup(cmd->pool, "QQQQQ");
    }
    c->trailer_encryption_len = strlen(c->trailer_encryption);
    c->pattern_encryption_end_tag = apr_strmatch_precompile(cmd->pool, c->trailer_encryption, 0);
    return NULL;
}

const char * set_type_obfuscation(cmd_parms *cmd, void *mconfig, const char *arg) {

    canonical_set_filter_config *c = (canonical_set_filter_config *)mconfig;

    if (arg != NULL) {
    	c->type_obfuscation = apr_pstrdup(cmd->pool, arg);
    }
    else {
        c->type_obfuscation = NULL;
    }

    return NULL;

}
const char * set_sitemapobfuscate(cmd_parms *cmd, void *mconfig, const char *arg) {

    canonical_set_filter_config *c = (canonical_set_filter_config *)mconfig;

    if (arg != NULL) {
    	c->sitemap_obfuscate = apr_pstrdup(cmd->pool, arg);
    }
    else {
        c->sitemap_obfuscate = NULL;
    }

    return NULL;

}

const char * set_activate(cmd_parms *cmd, void *mconfig, const char *arg) {

    canonical_set_filter_config *c = (canonical_set_filter_config *)mconfig;

    if (arg == NULL) {
        c->activate = 0;
    }
    else {
        if (apr_strnatcmp(arg, "0") == 0)
            c->activate = 0;
        else
            c->activate = 1;
    }

    return NULL;

}

const char * set_excepted_uri(cmd_parms *cmd, void *mconfig, const char *arg) {

    canonical_set_filter_config *c = (canonical_set_filter_config *)mconfig;

    if (arg == NULL)
        return NULL;

    apr_strmatch_pattern *newpattern = apr_strmatch_precompile(cmd->pool, arg, 0);

    *(const char**)apr_array_push(c->excepted_uri) = apr_pstrdup(cmd->pool, arg);
    *(const apr_strmatch_pattern **)apr_array_push(c->pattern_excepted_uri) = newpattern;

    return NULL;

}

static int canonical_set_post_config(apr_pool_t *p, apr_pool_t *plog,
				     apr_pool_t *ptemp, server_rec *s)
{
	pattern_head_start_tag = apr_strmatch_precompile(p, head_start_tag, 0);
	pattern_head_end_tag = apr_strmatch_precompile(p, head_end_tag, 0);
	// pattern to match tag canonical
	pattern_link_can_start_tag = apr_strmatch_precompile(p, link_can_start_tag, 0);
	pattern_link_can_end_tag = apr_strmatch_precompile(p, link_can_end_tag, 0);
	pattern_link_can_middle_tag = apr_strmatch_precompile(p, link_can_middle_tag, 0);
	pattern_link_can_middle1_tag = apr_strmatch_precompile(p, link_can_middle1_tag, 0);
	pattern_link_end_tag = apr_strmatch_precompile(p, link_end_tag, 0);
	pattern_href_attr = apr_strmatch_precompile(p, href_attr, 0);
	pattern_quote = apr_strmatch_precompile(p, quote, 0);
	pattern_dquote = apr_strmatch_precompile(p, dquote, 0);
  	pattern_question_mark_tag = apr_strmatch_precompile(p, question_mark_tag, 0);
    pattern_html_mime = apr_strmatch_precompile(p, html_mime, 0);
    pattern_text_xml = apr_strmatch_precompile(p, text_xml, 0);
    pattern_urlset_tag = apr_strmatch_precompile(p, urlset_tag, 0);
    pattern_loc_start_tag = apr_strmatch_precompile(p, loc_start_tag, 0);
    pattern_loc_end_tag = apr_strmatch_precompile(p, loc_end_tag, 0);
    pattern_generic_end_tag = apr_strmatch_precompile(p, generic_end_tag, 0);

	return OK;
}


